#define _DEFAULT_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem.h"
#include "mem_internals.h"

#define START_TEST(test){printf("starting test: " #test "\n"); do_test(test);}

#define ASSERT_TEST_CONDITION(condition){ \
    if(condition){                   \
        printf("test passed\n");      \
    }else{printf("test failed\n");} \
}

void do_test(void (*test)(void *heap)) {
    void *heap = heap_init(0);
    test(heap);
    heap_term();
}

void test_mem_alloc_free(void *heap) {
    (void) heap;
    int *var = _malloc(sizeof(int));
    ASSERT_TEST_CONDITION(var != NULL)
    _free(var);
}

void test_free_one_block(void *heap) {
    (void) heap;
    // чтобы проверить, что блок освободился - выделим память, освободим, выделим снова и сравним указатели
    int *var1 = _malloc(sizeof(int));
    _free(var1);
    int *var2 = _malloc(sizeof(int));
    ASSERT_TEST_CONDITION(var1 == var2)
    _free(var2);
}

void test_free_two_blocks(void *heap) {
    (void) heap;
    // для двух блоков - аналогично, но теперь выделяем два раза и два раза чистим, чтобы таким образом образовалось два блока
    int *var1 = _malloc(sizeof(int));
    int *var2 = _malloc(sizeof(int));
    _free(var1);
    _free(var2);
    int* var3 = _malloc(sizeof (int) * 2);
    ASSERT_TEST_CONDITION(var3 == var1)
    _free(var3);
}

void test_alloc_new_region_extend(void *heap) {
    //сохраняем сколько в регионе было памяти в начале (ее там примерно REGION_MIN_SIZE - sizeof(struct region)) и смотрим расширилась она или нет
    struct region* region = (struct region*) heap;
    size_t initial_size = region->size;
    _malloc(2*REGION_MIN_SIZE);
    ASSERT_TEST_CONDITION(initial_size < region->size)
}

void test_alloc_new_region_new_place(void *heap) {
    // не очень понятно что делать здесь, но попробуем инициализировать кучу,
    // замапить регион сразу после кучи(а так как куча инициализируется через heap_init(0), то размер ее будет REGION_MIN_SIZE)
    // попытаться выделить регион размером 2хREGION_MIN_SIZE, он выделится где-то там после замапленного региона
    // сравнить что новый регион - объявлен как нехт в первоначальном
    struct region* region = (struct region*) heap;
    void* new_region = mmap(heap + REGION_MIN_SIZE, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED | MAP_FIXED_NOREPLACE, -1, 0);
    void* detached_region = _malloc(2*REGION_MIN_SIZE);
    ASSERT_TEST_CONDITION((detached_region == (void*)((struct block_header*) region)->next + offsetof(struct block_header, contents)))
    munmap(new_region, REGION_MIN_SIZE);
}

int main() {
    START_TEST(test_mem_alloc_free)
    START_TEST(test_free_one_block)
    START_TEST(test_free_two_blocks)
    START_TEST(test_alloc_new_region_extend)
    START_TEST(test_alloc_new_region_new_place)
}